public class X
{
    public static func log(fileName: String = #file, functionName: String = #function)
    {
        print()
        print("class name: X")
        print("function name: \(functionName)")
        print("file name: \(fileName)")
        print()
    }
}